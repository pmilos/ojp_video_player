#pragma once

namespace OJP_VIDEO_PLAYER {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: AxWMPLib::AxWindowsMediaPlayer^  axWindowsMediaPlayer1;
	private: System::Windows::Forms::Button^  btnPlay;
	private: System::Windows::Forms::Button^  btnStop;
	private: System::Windows::Forms::Button^  btnBrowse;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::Button^  btnPause;
	private: System::Windows::Forms::Button^  button1;


	private: System::Windows::Forms::ToolTip^  toolTip1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  btnFullScreen;
	private: System::ComponentModel::IContainer^  components;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->axWindowsMediaPlayer1 = (gcnew AxWMPLib::AxWindowsMediaPlayer());
			this->btnPlay = (gcnew System::Windows::Forms::Button());
			this->btnStop = (gcnew System::Windows::Forms::Button());
			this->btnBrowse = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->btnPause = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->btnFullScreen = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->axWindowsMediaPlayer1))->BeginInit();
			this->SuspendLayout();
			// 
			// axWindowsMediaPlayer1
			// 
			this->axWindowsMediaPlayer1->Enabled = true;
			this->axWindowsMediaPlayer1->Location = System::Drawing::Point(28, 36);
			this->axWindowsMediaPlayer1->Name = L"axWindowsMediaPlayer1";
			this->axWindowsMediaPlayer1->OcxState = (cli::safe_cast<System::Windows::Forms::AxHost::State^>(resources->GetObject(L"axWindowsMediaPlayer1.OcxState")));
			this->axWindowsMediaPlayer1->Size = System::Drawing::Size(590, 259);
			this->axWindowsMediaPlayer1->TabIndex = 0;
			// 
			// btnPlay
			// 
			this->btnPlay->Location = System::Drawing::Point(41, 304);
			this->btnPlay->Name = L"btnPlay";
			this->btnPlay->Size = System::Drawing::Size(75, 23);
			this->btnPlay->TabIndex = 1;
			this->btnPlay->Text = L"Play";
			this->btnPlay->UseVisualStyleBackColor = true;
			this->btnPlay->Click += gcnew System::EventHandler(this, &MyForm::btnPlay_Click);
			// 
			// btnStop
			// 
			this->btnStop->Location = System::Drawing::Point(203, 304);
			this->btnStop->Name = L"btnStop";
			this->btnStop->Size = System::Drawing::Size(75, 23);
			this->btnStop->TabIndex = 2;
			this->btnStop->Text = L"Stop";
			this->btnStop->UseVisualStyleBackColor = true;
			this->btnStop->Click += gcnew System::EventHandler(this, &MyForm::btnStop_Click);
			// 
			// btnBrowse
			// 
			this->btnBrowse->Location = System::Drawing::Point(556, 7);
			this->btnBrowse->Name = L"btnBrowse";
			this->btnBrowse->Size = System::Drawing::Size(75, 23);
			this->btnBrowse->TabIndex = 3;
			this->btnBrowse->Text = L"Browse...";
			this->toolTip1->SetToolTip(this->btnBrowse, L"Search for files with extension wmv or avi");
			this->btnBrowse->UseVisualStyleBackColor = true;
			this->btnBrowse->Click += gcnew System::EventHandler(this, &MyForm::btnBrowse_Click);
			// 
			// textBox1
			// 
			this->textBox1->Enabled = false;
			this->textBox1->Location = System::Drawing::Point(48, 9);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(502, 20);
			this->textBox1->TabIndex = 4;
			this->textBox1->Text = L"SEARCH THE FILE ->";
			this->textBox1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// btnPause
			// 
			this->btnPause->Location = System::Drawing::Point(122, 304);
			this->btnPause->Name = L"btnPause";
			this->btnPause->Size = System::Drawing::Size(75, 23);
			this->btnPause->TabIndex = 5;
			this->btnPause->Text = L"Pause";
			this->btnPause->UseVisualStyleBackColor = true;
			this->btnPause->Click += gcnew System::EventHandler(this, &MyForm::btnPause_Click);
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(340, 304);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"FFW";
			this->toolTip1->SetToolTip(this->button1, L"Fast Forward (in choosen momment press Pause buton ,then Play)");
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// toolTip1
			// 
			this->toolTip1->ToolTipIcon = System::Windows::Forms::ToolTipIcon::Info;
			this->toolTip1->ToolTipTitle = L"Help";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(422, 303);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 7;
			this->button2->Text = L"FRV";
			this->toolTip1->SetToolTip(this->button2, L"Fast Reverse (in choosen momment press Pause buton ,then Play)");
			this->button2->UseVisualStyleBackColor = true;
			// 
			// btnFullScreen
			// 
			this->btnFullScreen->Location = System::Drawing::Point(556, 303);
			this->btnFullScreen->Name = L"btnFullScreen";
			this->btnFullScreen->Size = System::Drawing::Size(75, 23);
			this->btnFullScreen->TabIndex = 8;
			this->btnFullScreen->Text = L"FullScreen";
			this->btnFullScreen->UseVisualStyleBackColor = true;
			this->btnFullScreen->Click += gcnew System::EventHandler(this, &MyForm::btnFullScreen_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(695, 334);
			this->Controls->Add(this->btnFullScreen);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->btnPause);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->btnBrowse);
			this->Controls->Add(this->btnStop);
			this->Controls->Add(this->btnPlay);
			this->Controls->Add(this->axWindowsMediaPlayer1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"MyForm";
			this->Text = L"Video Player";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->axWindowsMediaPlayer1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void btnPlay_Click(System::Object^  sender, System::EventArgs^  e) {
		axWindowsMediaPlayer1->Ctlcontrols->play();
	}
	private: System::Void btnStop_Click(System::Object^  sender, System::EventArgs^  e) {
		axWindowsMediaPlayer1->Ctlcontrols->stop();
	}
	private: System::Void btnBrowse_Click(System::Object^  sender, System::EventArgs^  e) {

		Stream ^myStream = nullptr;
		OpenFileDialog ^openFileDialog1 = gcnew OpenFileDialog();
		openFileDialog1->InitialDirectory = "C:\\";
		openFileDialog1->Filter = "WMV Video File (*.wmv)|*.wmv|Windows Media Video File (*.avi)|*.avi|All Files (*.*)|*.*";
		openFileDialog1->FilterIndex = 1;
		openFileDialog1->RestoreDirectory = false;
		openFileDialog1->Multiselect = false;

		if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			if ((myStream = openFileDialog1->OpenFile()) != nullptr)
			{
				String^ strfilename = openFileDialog1->FileName;
				this->textBox1->Text = strfilename;
				axWindowsMediaPlayer1->URL = strfilename;
				myStream->Close();
			}
		}

	}
	private: System::Void btnPause_Click(System::Object^  sender, System::EventArgs^  e) {
		axWindowsMediaPlayer1->Ctlcontrols->pause();
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		axWindowsMediaPlayer1->Ctlcontrols->fastForward();
	}

	private: System::Void btnFullScreen_Click(System::Object^  sender, System::EventArgs^  e) {
		if (axWindowsMediaPlayer1->playState == WMPLib::WMPPlayState::wmppsPlaying)
		{
			MessageBox::Show(L"Double mouse click on the screen returns you from fullscreen mode");
			axWindowsMediaPlayer1->fullScreen = true;
		}
		
	}
};
}